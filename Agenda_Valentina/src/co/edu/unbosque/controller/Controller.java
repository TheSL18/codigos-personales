package co.edu.unbosque.controller;

import co.edu.unbosque.model.Agenda;
import co.edu.unbosque.view.*;

public class Controller {
	
	private VistaVentanas vista;
	private Agenda agenda;
	public Controller() {
		vista = new VistaVentanas();
		agenda = new Agenda ( 400 );
		funcionar();
	}
	
	public void funcionar() {
		
		int opcion = vista.leerDatoEntero("..: AGENDA DE CONTACTOS DE VALENTINA :..\n\nQue deseas hacer ??? :)\n    1. Agregar amigo\n"
				+ "    2. Ver listado\n    3. Buscar amigo por nombre\n    4. Modificar amigo\n"
				+ "    5. Eliminar amigo\n    6. Salir\n\nTu opcion es: ");
		
		String n = "";
		String c = "";
		long t = 0;
		String rta = "";
		switch (opcion) {
		case 1:
			n = vista.leerDatoString("Ingrese nombre de amigo: ");
			n = n.toUpperCase();
			c = vista.leerDatoString("Ingrese correo electronico de amigo: ");
			t = vista.leerDatoLong("Ingrese numero telefonico del amigo:");
			if(agenda.insertarContacto(n, c, t))
				vista.mostrarInformacion("Contacto insertado correctamente");
			else
				vista.mostrarInformacion("Contacto no se inserto correctamente");
			
			break;
		case 2:
			vista.mostrarInformacion("Mis amigos son : \n\n" + agenda.verListaContactos());
			break;
		case 3:
			n = vista.leerDatoString("Ingrese nombre de amigo que quiere ver: ");
			n = n.toUpperCase();
			rta = agenda.buscarAmigoPorNombre(n);
			if(!"".equals(rta))
				vista.mostrarInformacion("Los datos de mi amigo son:\n\n " + rta);
			else
				vista.mostrarInformacion("Contacto no se encuentra en la lista");
			break;
		case 4:
			n = vista.leerDatoString("Ingrese nombre de amigo a modificar: ");
			n = n.toUpperCase();
			int pos = agenda.buscarAmigoNombre(n);
			if (pos == -1)
				vista.mostrarInformacion("Contacto no se modifico, ya que no se encuentra en la agenda");
			else {
				c = vista.leerDatoString("Ingrese correo electronico de amigo: ");
				t = vista.leerDatoLong("Ingrese numero telefonico del amigo:");
				agenda.modificarAmigoPorNombre(n, c, t, pos);
				vista.mostrarInformacion("Contacto modificado correctamente");
			}
			break;
		case 5:
			n = vista.leerDatoString("Ingrese amigo del amigo a eliminar");
			n = n.toUpperCase();
			agenda.eliminarAmigo(n);
				agenda.eliminarAmigo(n);
			break;
		case 6:
			vista.mostrarInformacion("Hasta pronto Valentina :)");
			break;
		default:
			vista.mostrarInformacion("Opcion invalida Valentina");
			break;
		}
		
		if(opcion != 6)
			funcionar();
		
	}
	
}
