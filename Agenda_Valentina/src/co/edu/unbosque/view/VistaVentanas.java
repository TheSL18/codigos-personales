package co.edu.unbosque.view;

import javax.swing.JOptionPane;

public class VistaVentanas {
	
	public VistaVentanas() {
		
	}
	
	public int leerDatoEntero(String mensaje) {
		int dato = Integer.parseInt(JOptionPane.showInputDialog(mensaje));
		return dato;
	}
	
	public long leerDatoLong(String mensaje) {
		long dato = Long.parseLong(JOptionPane.showInputDialog(mensaje));
		return dato;
	}
	
	public String leerDatoString(String mensaje) {
		String dato = JOptionPane.showInputDialog(mensaje);
		return dato;
	}
	
	public double leerDatoReal(String mensaje) {
		double dato = Double.parseDouble(JOptionPane.showInputDialog(mensaje));
		return dato;
	}
	
	public void mostrarInformacion(String mensaje) {
		JOptionPane.showMessageDialog(null, mensaje);
	}

}
