package co.edu.unbosque.controller;

import javax.swing.JOptionPane;
import co.edu.unbosque.model.Numero;
import co.edu.unbosque.view.*;

//clase controlador
public class Controller {
	private Numero num;
	private VistaVentana vista;
	// private VistaConsola vista;
	
//constructor
	public Controller() {
		vista = new VistaVentana();
		funcionar();
	}
	
//Constructor con parametros
	public void funcionar() {
        int d = vista.leerDato("Ingrese un numero entero positivo entre 2 y 99999999: ");
        num = new Numero(d);
        if(d<=1 || d > 99999999) {
            vista.mostrarInformacion(" estimado usuario usted ingreso un dato erroneo ");
            funcionar();
        } else {
            vista.mostrarInformacion("Los numeros divisores\n" + num.primos());
            vista.mostrarInformacion("Las potencias son \n" + num.potencias(num));
            vista.mostrarInformacion("Usando el axioma\n" + num.axioma(num));
            JOptionPane.showMessageDialog(null,"Desarrollado por Luisa Enciso, Sergio Sanabria y Kevin Munoz");
        }
    }
}